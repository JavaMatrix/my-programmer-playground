package org.bitbukkit.javamatrix.thingies;

import java.util.ArrayList;

import org.bitbukkit.javamatrix.Main;
import org.bitbukkit.javamatrix.thingies.CooperativeBytleSwarm.Pheromone;
import org.bukkit.javamatrix.glhelper.Box;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Vector2f;

public class CooperativeBytle implements Thingy {

	Vector2f position;
	Vector2f velocity;
	float squadSize = 300;
	CooperativeBytleSwarm buddies;
	public Main world;
	public float speed = 3.0f;
	public float size = 10.0f;
	Nibble carrying = null;

	public CooperativeBytle(Vector2f pos, CooperativeBytleSwarm swarm,
			Main owner) {
		position = pos;
		velocity = new Vector2f(1, 0);
		buddies = swarm;
		world = owner;
	}

	@Override
	public void init() {

	}

	@Override
	public void update() {
		float swarmHeading = (3 * buddies.getHeadingPheromone(this) + buddies.desiredHeadingBoids(this)) / 4;

		Vector2f foodVec = findFood();

		float desiredHeading = (foodVec == null) ? swarmHeading : calcHeading(foodVec,
				swarmHeading);
		
		if (carrying != null) {
			desiredHeading = (float) new Vector2f(position).sub(world.nest.position).getTheta() + 180.0f;
			desiredHeading %= 360;
			speed = buddies.calcSpeed(carrying);
		}
		
		float heading = (float) velocity.getTheta();
		
		if (heading != desiredHeading) {
			float deltaHeading = desiredHeading - heading;
			if (Math.abs(deltaHeading) < 15.0) {
				heading = desiredHeading;
			} else {
				heading += 15.0f * Math.signum(deltaHeading);
			}
		}

		velocity.setTheta(heading);
		velocity.normalise();
		velocity.scale(speed);

		position.add(velocity);
		
		if (!world.things.contains(carrying)) {
			if (buddies.numHolding.containsKey(carrying)) {
				buddies.numHolding.remove(carrying);
			}
			
			carrying = null;
			speed = 3.0f;
		}

		for (Thingy thing : world.things) {
			if (thing instanceof Nibble && carrying == null) {
				Nibble snack = (Nibble) thing;
				Rectangle me = new Rectangle(position.x - size / 2, position.y
						- size / 2, size, size);
				Rectangle that = new Rectangle(snack.position.x - snack.value
						/ 100.0f, snack.position.y - snack.value / 100.0f,
						snack.value / 50.0f, snack.value / 50.0f);
				if (me.intersects(that)) {
					carrying = snack;
					buddies.addHolder(snack);
				}
			}
		}
		
		if (position.x >= 800) position.x -= 800;
		if (position.y >= 600) position.y -= 600;
		if (position.x <= 0) position.x += 800;
		if (position.y <= 0) position.y += 600;
		
		if (carrying != null) {
			carrying.setPosition(position);
		}
	}

	private float calcHeading(Vector2f foodVec, float swarmHeading) {
		return (float) ((180 * foodVec.getTheta() + foodVec.length() * swarmHeading) / (foodVec
				.length() + 180));
	}

	private Vector2f findFood() {
		
		if (carrying != null) {
			return null;
		}
		
		Nibble datFood = null;
		float dist = 180;

		for (Thingy thing : world.things) {
			if (thing instanceof Nibble) {
				Nibble snack = (Nibble) thing;
				float distance = new Vector2f(position).sub(snack.position)
						.length();
				if (distance < dist) {
					datFood = snack;
					dist = distance;
				}
			}
		}

		if (datFood == null) {
			return null;
		}

		return new Vector2f(position).sub(datFood.position).negateLocal();
	}

	@Override
	public void draw() {
		Box daBox = new Box(buddies.getColor(), position, size, (float) velocity.getTheta());
		daBox.blit();
		//GL11.glColor3f(0, 0, 0);
		//GLHelper.drawString("X " + position.x + ", Y " + position.y, (int) position.x, (int) position.y);
	}

	@Override
	public void stop() {
		
	}

	@Override
	public void setPosition(Vector2f position) {
		this.position = position;
	}

	public void mark() {
		for (Pheromone p : new ArrayList<Pheromone>(buddies.markers)) {
			if (p.owner == this) {
				buddies.markers.remove(p);
			}
		}
		if (speed < 2.9) {
			if (carrying != null) {
				buddies.markers.add(new Pheromone(position, (int) ((3.0 - speed) * 10000), this));
			} else {
				speed = 3.0f;
			}
		}
	}
}
