package org.bitbukkit.javamatrix.thingies;

import java.util.ArrayList;
import java.util.HashMap;

import org.bitbukkit.javamatrix.Main;
import org.bukkit.javamatrix.glhelper.Box;
import org.lwjgl.Sys;
import org.lwjgl.opengl.GL11;
import org.newdawn.slick.Color;
import org.newdawn.slick.geom.Vector2f;

public class CooperativeBytleSwarm {

	public static Main world;
	public double daSwarmRandom = Math.random();
	public float neighborhoodSize = 10;
	public float neighborhoodAngle = 360;
	public float[] weights = { 1, 1, 1 };
	public static final int WEIGHT_SEPARATION = 0;
	public static final int WEIGHT_ALIGNMENT = 1;
	public static final int WEIGHT_COHESION = 2;
	public int desiredSquadSize = 10;
	public HashMap<Nibble, Integer> numHolding = new HashMap<Nibble, Integer>();
	public ArrayList<Pheromone> markers = new ArrayList<Pheromone>();
	
	public static class Pheromone {
		public int strength;
		public Vector2f position;
		public CooperativeBytle owner;
		
		public Pheromone(Vector2f where, int howMuch, CooperativeBytle who) {
			strength = howMuch;
			position = where;
			owner = who;
		}
	}

	public ArrayList<CooperativeBytle> getSwarmMembers() {
		ArrayList<CooperativeBytle> daSwarm = new ArrayList<CooperativeBytle>();
		for (Thingy t : world.things) {
			if (t instanceof CooperativeBytle) {
				daSwarm.add((CooperativeBytle) t);
			}
		}

		return daSwarm;
	}
	
	public void addHolder(Nibble n) {
		if (numHolding.containsKey(n)) {
			Integer i = numHolding.get(n);
			numHolding.remove(n);
			numHolding.put(n, Integer.valueOf(i + 1));
		} else {
			numHolding.put(n, 1);
		}
	}
	
	public float calcSpeed(Nibble n) {
		float weight = n.value / 500.0f;
		if (numHolding.containsKey(n)) {
			return (float) Math.max(0.05, 3.0f - (weight / numHolding.get(n)));
		}
		return (float) Math.max(0.05, 3.0 - weight);
	}

	public Color getColor() {
		float t = ((float) daSwarmRandom) * Sys.getTime() / 1000;
		return new Color((float) (Math.sin(t) + 1) / 2,
				(float) (Math.cos(t) + 1) / 2, (float) ((t % 255) / 510));
	}

	public float getDistance(Vector2f point1, Vector2f point2) {
		return (new Vector2f(point1).sub(point2).length());
	}

	public double getTheta(Vector2f point1, Vector2f point2) {
		return (new Vector2f(point1).sub(point2).getTheta());
	}

	public float desiredHeadingBoids(CooperativeBytle beetle) {
		float avgHeading = 0;
		float nearX = 0;
		float nearY = 0;
		float centerX = 0;
		float centerY = 0;
		int numSquadmates = 0;
		int numNeighbors = 0;

		ArrayList<CooperativeBytle> flockmates = getSwarmMembers();
		flockmates.remove(beetle); // Don't pay attention to self

		// Gather the data
		for (CooperativeBytle flockmate : flockmates) {
			avgHeading += flockmate.velocity.getTheta();
			if (getDistance(beetle.position, flockmate.position) <= beetle.squadSize) {
				centerX += flockmate.position.x;
				centerY += flockmate.position.y;
				numSquadmates++;

				if (getDistance(beetle.position, flockmate.position) <= neighborhoodSize) {
					double angleDiff = beetle.position.getTheta()
							- getTheta(beetle.position, flockmate.position);
					if (Math.abs(angleDiff) <= neighborhoodAngle) {
						numNeighbors++;
						nearX += flockmate.position.x;
						nearY += flockmate.position.y;
					}
				}
			}
		}

		if (numSquadmates > 0) {
			avgHeading /= numSquadmates;
			centerX /= numSquadmates;
			centerY /= numSquadmates;
		} else {
			avgHeading = (float) getTheta(beetle.position, new Vector2f(400,
					300));
			centerX = 400;
			centerY = 300;
		}

		if (numNeighbors > 0) {
			nearX /= numNeighbors;
			nearY /= numNeighbors;
		}

		// Separation
		Vector2f separation = new Vector2f(beetle.position).sub(
				new Vector2f(nearX, nearY));

		// Cohesion
		Vector2f cohesion = new Vector2f(beetle.position).sub(new Vector2f(
				centerX, centerY)).negateLocal();
		
		// Add them together
		float total = (float) (separation.getTheta() * weights[WEIGHT_SEPARATION] +
							   avgHeading            * weights[WEIGHT_ALIGNMENT] +
							   cohesion.getTheta()   * weights[WEIGHT_COHESION]);
		// And divide by total weight.
		total /= weights[WEIGHT_SEPARATION] + weights[WEIGHT_ALIGNMENT] + weights[WEIGHT_COHESION];
		
		// Mess with squad radius to get a good squad size
		if (numSquadmates < desiredSquadSize) {
			beetle.squadSize++;
		} else if (numSquadmates > desiredSquadSize) {
			beetle.squadSize--;
		}
		
		return total;
	}
	
	public void grabPheromones() {
		for (Pheromone p : new ArrayList<Pheromone>(markers)) {
			p.strength--;
			if (p.strength <= 0) {
				markers.remove(p);
			}
		}
		for (CooperativeBytle b : getSwarmMembers()) {
			b.mark();
		}
	}
	
	public void drawPheromones() {
		for (Pheromone p : markers) {
			Box daBox = new Box(new Color(0f, 0f, 0f, 0.5f), p.position, p.strength / 500.0f, 0);
			daBox.blit();
			GL11.glBegin(GL11.GL_LINE);
				GL11.glVertex2f(p.position.x, p.position.y);
				GL11.glVertex2f(p.owner.position.x, p.owner.position.y);
			GL11.glEnd();
		}
	}
	
	public float getHeadingPheromone(CooperativeBytle beetle) {
		float maxSmell = 0;
		Pheromone max = null;
		for (Pheromone p : markers) {
			if (p.owner == beetle) continue;
			float smell = p.strength;
			if (smell > maxSmell) {
				maxSmell = smell;
				max = p;
			}
		}
		
		if (max == null) {
			return (float) beetle.velocity.getTheta();
		}
		
		return (float) (getTheta(beetle.position, max.position) + 180) % 360;
	}
}
