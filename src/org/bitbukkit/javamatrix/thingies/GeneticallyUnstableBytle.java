package org.bitbukkit.javamatrix.thingies;

import java.util.ArrayList;

import org.bitbukkit.javamatrix.Main;
import org.bukkit.javamatrix.glhelper.Box;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;
import org.newdawn.slick.Color;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Vector2f;

public class GeneticallyUnstableBytle implements Thingy {

	public abstract class State {
		public void preAct(GeneticallyUnstableBytle actor) {
		};

		public State switchStates(GeneticallyUnstableBytle actor) {
			return this;
		}

		public void act(GeneticallyUnstableBytle actor) {
		};
	}

	public class StateWander extends State {
		@Override
		public void act(GeneticallyUnstableBytle actor) {
			if (actor.arrived || actor.goal == null) {
				actor.setGoal(new Vector2f(world.rnumgen
						.nextInt(Main.SCREEN_WIDTH - (int) actor.size) + actor.size / 2,
						world.rnumgen.nextInt(Main.SCREEN_HEIGHT - (int) actor.size)
								+ actor.size / 2));
			}
		}

		@Override
		public State switchStates(GeneticallyUnstableBytle actor) {
			return (Keyboard.isKeyDown(Keyboard.KEY_PERIOD)) ? new FollowDatMouse()
					: ((!actor.nearby.isEmpty()) ? new GoToFood() : this);
		}
	}

	public class GoToFood extends State {
		@Override
		public void act(GeneticallyUnstableBytle actor) {
			float minDist = Float.MAX_VALUE;
			Nibble nearest = null;
			for (Nibble food : actor.nearby) {
				float dist = new Vector2f(food.position).sub(actor.position)
						.length();
				if (dist / food.value < minDist) {
					minDist = dist / food.value;
					nearest = food;
				}
			}
			if (nearest != null) {
				actor.setGoal(nearest.position);
			} else {
				actor.goal = actor.position;
			}
		}

		@Override
		public State switchStates(GeneticallyUnstableBytle actor) {
			return (actor.nearby.isEmpty()) ? new StateWander() : this;
		}
	}

	public class FollowDatMouse extends State {
		@Override
		public void act(GeneticallyUnstableBytle actor) {
			if (Mouse.isButtonDown(0)) {
				actor.setGoal(new Vector2f(Mouse.getX(), Mouse.getY()));
			}
		}

		@Override
		public State switchStates(GeneticallyUnstableBytle actor) {
			return (Keyboard.isKeyDown(Keyboard.KEY_COMMA)) ? new StateWander()
					: this;
		}
	}

	public Main world;
	public State currentState;
	public Vector2f position;
	private Vector2f goal;
	public float speed = 3.0f;
	public boolean arrived = false;
	public Color color = Color.blue;
	public float size = 16;
	public static final boolean DRAW_GOALS = false;
	public boolean rotateToGoal = true;
	public float angle;
	public boolean angleMatch;
	public float angleSpeed = 15.0f;
	public float health = 600;
	public static final int MAX_HEALTH = 600;
	private static final boolean DRAW_HEALTH = false;
	public ArrayList<Nibble> nearby = new ArrayList<Nibble>();
	public long age = 0;
	private double fov = 360;
	public int generation = 0;

	public void setGoal(Vector2f value) {
		if (!value.equals(goal)) {
			goal = value;
			arrived = false;
			angleMatch = false;
		}
	}

	public GeneticallyUnstableBytle(Main theWorld, Vector2f thePlace) {
		world = theWorld;
		currentState = new StateWander();
		position = thePlace;
	}

	@Override
	public void init() {
		// Nothing to do here
	}

	@Override
	public void update() {
		currentState = currentState.switchStates(this);
		currentState.preAct(this);
		currentState.act(this);

		// Update position

		if (goal == null) {
			goal = position;
		}

		Vector2f dist = new Vector2f(goal.x - position.x, goal.y - position.y);

		if (rotateToGoal && !angleMatch) {
			float da = ((float) dist.getTheta()) - angle;
			if (Math.abs(da) > angleSpeed) {
				if (Math.abs(da) > 180) {
					angle -= angleSpeed * Math.signum(da);
					if (angle < 0) {
						angle += 360;
					}
					angle %= 360;
				} else {
					angle += angleSpeed * Math.signum(da);
				}
			} else {
				angle = (float) dist.getTheta();
				angleMatch = true;
			}
			return;
		}

		if (dist.length() > speed) {
			dist.normalise();
			dist.scale(speed);
			position.add(dist);
		} else {
			position.add(dist);
			arrived = true;
		}
		if (health > 0) {
			health -= 0.1 + speed / 20.0f;
			//health -= 1;
		} else {
			world.dead.add(this);
		}

		nearby.clear();
		for (Thingy thing : world.things) {
			if (thing instanceof Nibble) {
				Rectangle me = new Rectangle(position.x - size / 2, position.y
						- size / 2, size, size);
				float foodSize = ((Nibble) thing).value / 50.0f;
				Rectangle that = new Rectangle(((Nibble) thing).position.x
						- foodSize / 2, ((Nibble) thing).position.y - foodSize
						/ 2, foodSize, foodSize);

				if (new Vector2f(((Nibble) thing).position).sub(position)
						.length() < 180
						&& Math.abs(new Vector2f(((Nibble) thing).position).sub(
								position).getTheta()
								- position.getTheta()) < fov ) {
					nearby.add((Nibble) thing);
				}

				if (me.intersects(that)) {
					health += Math.min(((Nibble) thing).value, 25.0);
					if (((Nibble) thing).value <= 0) {
						world.dead.add(thing);
						nearby.remove((Nibble) thing);
					}
					((Nibble) thing).value -= 25.0;
					if (health > MAX_HEALTH) {
						int healthLeft = (int) health / 2;
						health /= 2;
						while (healthLeft > 0) {
							GeneticallyUnstableBytle baby = new GeneticallyUnstableBytle(world, new Vector2f(
									position.x, position.y));
							baby.health = Math.min(healthLeft, MAX_HEALTH);
							float speedImprove = (float) Math.random()
									- (float) Math.random();
							baby.speed = this.speed + speedImprove;
							baby.fov = this.fov + Math.random() - Math.random();
							baby.color = this.mutateColor();
							baby.generation = this.generation + 1;
							baby.size = (float) (this.size - Math.random() + Math.random());
							baby.nearby.add((Nibble) thing);
							world.newThings.add(baby);
							healthLeft -= MAX_HEALTH;
						}
					}
				}
			}
		}
		age++;
	}
	
	public double limitTo1(double num) {
		return Math.max(Math.min(num, 1), 0);
	}
	
	public Color mutateColor() {
		float r = (float) limitTo1(Math.random());
		float g = (float) limitTo1(Math.random());
		float b = (float) limitTo1(Math.random());
		
		System.out.println();
		
		return new Color(r, g, b);
	}

	@Override
	public void draw() {
		// Draw a box at this position.
		Box daBox = new Box(color, position, health / MAX_HEALTH * 64 + 16, angle);
		daBox.blit();
		if (DRAW_GOALS) {
			GL11.glColor3f(1, 0, 0);
			GL11.glBegin(GL11.GL_LINES);
			GL11.glVertex2f(goal.x - 10, goal.y);
			GL11.glVertex2f(goal.x + 10, goal.y);
			GL11.glEnd();
			GL11.glBegin(GL11.GL_LINES);
			GL11.glVertex2f(goal.x, goal.y - 10);
			GL11.glVertex2f(goal.x, goal.y + 10);
			GL11.glEnd();
		}
		if (DRAW_HEALTH) {
			GL11.glColor3f(1, 0, 0);
			Vector2f healthBarBottomLeft = new Vector2f(position.x - (50 - size)
					/ 2, position.y - size / 2 - 25.0f);
			GL11.glBegin(GL11.GL_QUADS);
			GL11.glVertex2f(healthBarBottomLeft.x, healthBarBottomLeft.y);
			GL11.glVertex2f(healthBarBottomLeft.x, healthBarBottomLeft.y + 10);
			GL11.glVertex2f(healthBarBottomLeft.x + 50, healthBarBottomLeft.y + 10);
			GL11.glVertex2f(healthBarBottomLeft.x + 50, healthBarBottomLeft.y);
			GL11.glEnd();
			GL11.glColor3f(0, 1, 0);
			float healthBarPercent = ((float) health / MAX_HEALTH) * 50;
			GL11.glBegin(GL11.GL_QUADS);
			GL11.glVertex2f(healthBarBottomLeft.x, healthBarBottomLeft.y);
			GL11.glVertex2f(healthBarBottomLeft.x, healthBarBottomLeft.y + 10);
			GL11.glVertex2f(healthBarBottomLeft.x + healthBarPercent,
					healthBarBottomLeft.y + 10);
			GL11.glVertex2f(healthBarBottomLeft.x + healthBarPercent,
					healthBarBottomLeft.y);
			GL11.glEnd();
		}
	}

	public void stop() {
		// Nothing to do here
	}
	
	@Override
	public void setPosition(Vector2f position) {
		this.position = position;
	}
}
