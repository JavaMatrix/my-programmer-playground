package org.bitbukkit.javamatrix.thingies;

import java.util.ArrayList;

import org.bitbukkit.javamatrix.Main;
import org.bukkit.javamatrix.glhelper.Box;
import org.newdawn.slick.Color;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Vector2f;

public class VortexOfDeath implements Thingy {

	float angle = 0.0f;
	ArrayList<Color> c = new ArrayList<Color>();
	public Vector2f position = new Vector2f(400, 300);;
	public boolean drill = false;
	public float sizeMul = 1;
	public float storedFood = 0;
	
	public VortexOfDeath(int i, int j) {
		position = new Vector2f(i, j);
	}

	@Override
	public void init() {
		for (int a = 0; a < 360; a++) {
			float n = 1.0f - (float) Math.min((a / 360.0f) * 1.05, 1.0) * 1.1f;
			n = (float)Math.max(n, 0);
			
			@SuppressWarnings("unused")
			Color psycho = new Color((float)((Math.sin(a) + 1) / 2),
					(float)((Math.cos(a) + 1) / 2),
					// 2^-n(2^n-1)
					(float)(Math.pow(2, -a) * (Math.pow(2, a) - 1)),
					n);
			
			Color greyscale = new Color(n, n, n, n);
			
			c.add(greyscale);
		}
	}

	@Override
	public void update() {
		angle += (Main.getDelta() / 1000.0) * 360;
		angle %= 360;
	}

	@Override
	public void draw() {
		for (int a = 0; a < 360; a++) {
			Box daBox = new Box(c.get(a), position, (38 - ((20 + a) / 10.0f)) * sizeMul , angle + a);
			
			if (drill) {
				daBox = new Box(c.get(a), new Vector2f(position.x + a/5, position.y + a/5), 38 - ((20 + a) / 10.0f), angle + a);
			}
			
			
			daBox.blit();
		}
	}
	
	public boolean collides (Rectangle other) {
		float size = 38 * sizeMul / 2;
		Circle me = new Circle(position.x, position.y, size);
		return me.intersects(other);
	}

	@Override
	public void stop() {
		// Nothin' to do
	}

	@Override
	public void setPosition(Vector2f position) {
		this.position = position;
	}

}
