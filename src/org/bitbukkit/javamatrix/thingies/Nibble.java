package org.bitbukkit.javamatrix.thingies;

import org.bitbukkit.javamatrix.Main;
import org.bukkit.javamatrix.glhelper.Box;
import org.newdawn.slick.Color;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Vector2f;

public class Nibble implements Thingy {

	int value;
	Vector2f position;
	Main world;

	public Nibble(Vector2f pos, Main world) {
		position = pos;
		this.world = world;
	}

	@Override
	public void init() {
		value = (int) (Math.random() * 1800) + 25;
		// value = 50;
	}

	@Override
	public void update() {
		value++;
		if (world.nest != null && world.nest
				.collides(new Rectangle(this.position.x - value / 100.0f,
						this.position.y - value / 100.0f, value / 50.0f,
						value / 50.0f))) {
			world.nest.storedFood += value;
			world.dead.add(this);
		}
	}

	@Override
	public void draw() {
		Box daBox = new Box(new Color(0, 0.5f, 0), position, value / 50.0f, 0);
		daBox.blit();
	}

	@Override
	public void stop() {
		// Do nothing
	}

	@Override
	public void setPosition(Vector2f position) {
		this.position = position;
	}
}
