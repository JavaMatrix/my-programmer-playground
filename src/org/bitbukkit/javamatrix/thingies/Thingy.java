package org.bitbukkit.javamatrix.thingies;

import org.newdawn.slick.geom.Vector2f;

public interface Thingy {
	public void init();
	public void update();
	public void draw();
	public void stop();
	public void setPosition(Vector2f position);
}
