package org.bitbukkit.javamatrix;

import java.util.ArrayList;
import java.util.Random;

import org.bitbukkit.javamatrix.thingies.CooperativeBytle;
import org.bitbukkit.javamatrix.thingies.CooperativeBytleSwarm;
import org.bitbukkit.javamatrix.thingies.GeneticallyUnstableBytle;
import org.bitbukkit.javamatrix.thingies.Nibble;
import org.bitbukkit.javamatrix.thingies.Thingy;
import org.bitbukkit.javamatrix.thingies.VortexOfDeath;
import org.bukkit.javamatrix.glhelper.GLHelper;
import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.newdawn.slick.Color;
import org.newdawn.slick.geom.Vector2f;

public class Main {

	public static int SCREEN_WIDTH = 1280;
	public static int SCREEN_HEIGHT = 800;
	public static int MAX_FPS = 60;
	private static long lastFrame;
	private static long lastFPS;
	private static int fps;
	public ArrayList<Thingy> things = new ArrayList<Thingy>();
	public ArrayList<Thingy> dead = new ArrayList<Thingy>();
	public ArrayList<Thingy> newThings = new ArrayList<Thingy>();
	public Random rnumgen = new Random();
	int starvingTime = 0;
	public VortexOfDeath nest;

	public void start() {

		CooperativeBytleSwarm.world = this;

		try {
			Display.setDisplayMode(new DisplayMode(SCREEN_WIDTH, SCREEN_HEIGHT));
			Display.create();
			Display.setFullscreen(true);
		} catch (LWJGLException e) {
			e.printStackTrace();
			System.exit(0);
		}

		GLHelper.init2D(SCREEN_WIDTH, SCREEN_HEIGHT);
		GLHelper.bgColor = Color.white;

		GeneticallyUnstableBytle b = new GeneticallyUnstableBytle(this, new Vector2f(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2));
		things.add(b);

		startFPS();

		for (Thingy thing : things) {
			thing.init();
		}

		while (!Display.isCloseRequested()) {
			GLHelper.clearScreen();

			starvingTime--;

			if (Math.random() > 0.98 && starvingTime < 0) {
				Random r = new Random();
				Nibble food = new Nibble(new Vector2f(r.nextInt(SCREEN_WIDTH - 40) + 20,
						r.nextInt(SCREEN_HEIGHT - 40) + 20), this);
				food.init();
				things.add(food);
			}

			if (Math.random() > 0.99 && Math.random() > 0.99) {
				starvingTime = (int) (Math.random() * 2000);
			}

			for (Thingy newThing : newThings) {
				things.add(newThing);
			}
			newThings.clear();
			for (Thingy thing : things) {
				thing.update();
			}
			for (Thingy corpse : dead) {
				System.out.println("Killing " + corpse);
				things.remove(corpse);
			}
			dead.clear();

			int pop = 0;
			long totalAge = 0;
			float totalSpeed = 0;
			int maxGen = 0;
			int minGen = 1000000;

			for (Thingy thing : things) {
				thing.draw();
				if (thing instanceof GeneticallyUnstableBytle) {
					pop++;
					totalAge += ((GeneticallyUnstableBytle) thing).age;
					totalSpeed += ((GeneticallyUnstableBytle) thing).speed;
					if (((GeneticallyUnstableBytle) thing).generation > maxGen) {
						maxGen = ((GeneticallyUnstableBytle) thing).generation;
					} else if (((GeneticallyUnstableBytle) thing).generation < minGen) {
						minGen = ((GeneticallyUnstableBytle) thing).generation;
					}
				}
			}
			
			if (pop > 0) {
				GL11.glColor3f(0, 0, 0);
				GLHelper.drawString("Population: " + pop, 5, 585);
				GLHelper.drawString(String.format("Avg. Age: %10.1f sec.",
						(totalAge / pop) / 60.0), 5, 565);
				GLHelper.drawString(String.format("Avg. Speed: %10.1f px/f.",
						(totalSpeed / (float) pop)), 5, 545);
				GLHelper.drawString(
						String.format("Generations %d to %d", minGen, maxGen),
						5, 525);
			}

			if (starvingTime > 0) {
				GL11.glColor3f(1, 0, 0);
				GLHelper.drawString("STARVING TIME (" + starvingTime + ")", 5,
						505);
			}
			GL11.glColor3f(0, 0.75f, 0);
			
			Display.update();
			Display.sync(MAX_FPS);
			updateFPS();
		}
		Display.destroy();

		for (Thingy thing : things) {
			thing.stop();
		}
	}

	public static void startFPS() {
		// some startup code
		lastFPS = getTime(); // set lastFPS to current Time
	}

	/**
	 * Calculate the FPS and set it in the title bar
	 */
	public static void updateFPS() {
		if (getTime() - lastFPS > 1000) {
			Display.setTitle("FPS: " + fps);
			fps = 0; // reset the FPS counter
			lastFPS += 1000; // add one second
		}
		fps++;
	}

	public static long getTime() {
		return (Sys.getTime() * 1000) / Sys.getTimerResolution();
	}

	public static int getDelta() {
		long time = getTime();
		int delta = (int) (time - lastFrame);
		lastFrame = time;
		return delta;
	}

	public static void main(String[] args) {
		Main ex = new Main();
		ex.start();
	}
}
