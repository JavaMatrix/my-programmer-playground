package org.bukkit.javamatrix.glhelper;

import org.lwjgl.opengl.GL11;
import org.newdawn.slick.Color;
import org.newdawn.slick.geom.Vector2f;

public class Box {
	public Color color;
	public Vector2f middle;
	public float size;
	public float rotation;
	
	//region Getters Und Setters, Ja.
	
	public Color getColor() {
		return color;
	}
	
	public void setColor(Color value) {
		color = value;
	}
	
	public Vector2f getCenter() {
		return middle;
	}
	
	public void setCenter(Vector2f value) {
		middle = value;
	}
	
	public float getSize() {
		return size;
	}
	
	public void setSize(float value) {
		size = value;
	}
	
	public float getRotation() {
		return rotation;
	}
	
	public void setRotation(float value) {
		rotation = value;
	}
	//endregion
	
	public Box(Color hue, Vector2f center, float bigness, float spinniness) {
		color = hue;
		middle = center;
		size = bigness;
		rotation = spinniness;
	}
	
	public void blit() {
		// make a nice little rotation matrix.
		GL11.glPushMatrix();
		GL11.glTranslatef(middle.getX(), middle.getY(), 0.0f);
		GL11.glRotatef(rotation, 0, 0, 1);
		GL11.glTranslatef(-middle.getX(), -middle.getY(), 0.0f);
		 
		// set the color of the quad (R,G,B,A)
		GL11.glColor4f(color.r, color.g, color.b, color.a);
		 
		// draw quad
		GL11.glBegin(GL11.GL_QUADS);
		    GL11.glVertex2f(middle.x - size / 2, middle.y - size / 2);
		    GL11.glVertex2f(middle.x - size / 2, middle.y + size / 2);
		    GL11.glVertex2f(middle.x + size / 2, middle.y + size / 2);
		    GL11.glVertex2f(middle.x + size / 2, middle.y - size / 2);
		GL11.glEnd();
		GL11.glPopMatrix();
	}
}
